# ColorAlphabet #
ColorAlphabet a Python package to colour text on a terminal using
Paul Green-Armytage color alphabet.

### Paul Green-Armytage color alphabet ###
The paper "Paul Green-Armytage color alphabet" by Paul Green-Armytage
can be found at https://aic-color.org/resources/Documents/jaic_v5_06.pdf

## Limits ##
Tested only on xterm and Konsole terminals

### Who do I talk to? ###

* [Roberto Vidmar](mailto:rvidmar@inogs.it)
